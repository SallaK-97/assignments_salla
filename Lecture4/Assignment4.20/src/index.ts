
const numb = Number(process.argv[2]);
let numOne = 0;
let numTwo = 1;
let nextNum;

console.log("Fibonacci Sequence:");

for (let i = 1; i <= numb; i++) {
	console.log(numOne);
	nextNum = numOne + numTwo;
	numOne = numTwo;
	numTwo = nextNum;
}
