
//Assignment 4.16
const start = Number(process.argv[2]);
const end = Number(process.argv[3]);
const arr = [];

if(start < end) {
	for (let i:number = start; i <= end; i++) {
        
		arr.push(i);
	}
} else {
	for (let i: number = start; i >= end; i--) {
		arr.push(i);
	}
}

// tässä puolestaan mielestäni tällainen pidempi if/else jossa kaksi melkein samanlaista for-lausetta, on ihan paikallaan. Tää on paljon selkeämmin luettava kuin monet muut vastaukset tähän tehtävään.
console.log(arr);

