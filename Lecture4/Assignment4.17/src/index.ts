const str = (process.argv[2]);
function reverseString(word:string){
	return word.split("").reverse().join("").split(" ").reverse().join(" "); 
    // nyt ei taida tämä toimia ihan odotetusti. kokeilepa ajaa tää tolla esimerkin arvolla ja kato mitä tulee.
    // Sallan kommentti: Toimii, kunhan laittaa process.argv kokonaan yksien hipsujen sisään. Esmerkki lauseen kanssa tämä tapa toimi.
}
console.log(reverseString(str));
