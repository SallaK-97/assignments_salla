
function UpperCase(One = "testi"){ // funktiot ja parametrit pienellä kirjaimella
	const word = One.split(""); // split("") jakaa sanan yksittäisiksi kirjaimiksi. split(" ") jakaa lauseen sanoiksi 
	let product = "";

	for (let i = 0; i < word.length; i++){
		const newWord = word[i];
		product = product + newWord[0].toUpperCase() + newWord.substring(1) + " ";
	} 
	return product.trim();
   
}
console.log(UpperCase());
