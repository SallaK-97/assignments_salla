//Korjattu

const numb = (n: number):number =>{
	if (n === 1){
		return n;
	}
	else{
		return n*numb(n - 1);
	}
};
const n = 5;
console.log(numb(n));

// Jarin kommentti:
// tämä taitaa jäädä nyt ikuiseen looppiin, sillä jos n === 3, se on pienempi kuin 7, jolloin se kutsuu itseään arvolla 2, joka on pienempi kuin 7, ja se jatkaa sitten kohti miinus äärettömyyttä
