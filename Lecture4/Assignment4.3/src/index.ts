
//1.
function sumFunction(a: number, b: number){
	return a + b;
}
const sum = sumFunction(2, 3);
console.log(sum);
//2.
const sumTwo = function (c: number, d: number) {
	return c + d;
};
const calc = sumTwo(2,3);
console.log(calc);
//3.
const arrowSum = (e: number, f:number) =>{
	return e + f;
};
console.log(arrowSum(3,2));

// Miten näitä pitää muuttaa, jotta ne voivat ottaa myös sen 3 parametria?


