const competitors = ["Julia", "Mark", "Spencer", "Ann" , "John", "Joe"];
const ordinals = ["st", "nd", "rd", "th"];

for (let i = 0; i < competitors.length; i++) {
	const contestant = competitors[i];
    // tästä saa vähän toistoa pois ottamalla tuon place arvon ehdolliseen muuttujaan
    // const place = i > 3 ? ordinals[3] : ordinals[i]
    // sitten ei tarvi if-lausetta ja console.log riittää kun kirjotetaan kerran
	if(i > 3){
		const place = ordinals[3];
		console.log((i + 1) + place , "competitor was " + contestant );
	}
	else{
		const place = ordinals[i];
		console.log((i + 1) + place , "competitor was " + contestant );
	}
}
