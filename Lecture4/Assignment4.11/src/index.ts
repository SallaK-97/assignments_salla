const names = [
	"Murphy",
	"Hayden",
	"Parker",
	"Arden",
	"George",
	"Andie",
	"Ray",
	"Storm",
	"Tyler",
	"Pat",
	"Keegan",
	"Carroll"
];


const red =  function (nimet: string[], sep:string): string {
	const sum = nimet.reduce((acc, cur) => {
		return acc + (acc.length > 0 ? sep : "") + cur;
	}, "");
	return sum;
};

console.log(red(names,", "));