const word = (process.argv[2]);

function palindrome(text:string) {

	const reverseWord = text.split("").reverse().join("");

	if(text === reverseWord) {
		console.log("It is a palindrome");
	}
	else {
		console.log("It is not a palindrome");
	}
}
palindrome(word);

