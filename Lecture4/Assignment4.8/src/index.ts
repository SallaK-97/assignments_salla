//Mallivastaus

const words = [
	"city",
	"distribute",
	"battlefield",
	"relationship",
	"spread",
	"orchestra",
	"directory",
	"copy",
	"raise",
	"ice"
];

const reverse = (word: string) => {
	return word.split("").reverse().join("");
};
// hyvä, selkeästi nimetty apufunktio

function reverseList(list: Array<string>) {
	return list.map(word => reverse(word));
    // tän voi myös kirjoittaa 
    // return list.map(reverse)
    // koska reverse ja x => reverse(x) ovat käytännössä sama funktio
}

console.log(reverseList(words));

/*const words = [
	"city",
	"distribute",
	"battlefield",
	"relationship",
	"spread",
	"orchestra",
	"directory",
	"copy",
	"raise",
	"ice"
];
words.map(listName =>{
	console.log(listName.split());
}); */