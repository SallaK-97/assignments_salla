//Korjattu tehtävä

const dice = () => {
    const sides = (number: number) => {
        const random = Math.floor(Math.random() * number) + 1;
        return random;
    };
    return sides;
};
  
const diceSix = dice();
const diceEight = dice();
const damage = diceSix(6) + diceSix(6) + diceEight(8) + diceEight(8);
console.log(damage);
  
//Jarin  alkuperäiset kommentit:
// Tästä puuttuu nyt vielä tehtävänannossa pyydetty
// "dice generator function that returns dice functions"
// eli tässä tehtävässä tärkeää on ymmärtää, miten rakennetaan funktio, joka palauttaa (generoi) uusia funktioita.
// Ehdotan, että teet tämän ajatuksella ja ajan kanssa uudestaan. Jos tulee tenkkapoo, niin laita viestiä ja katsotaan yhdessä.
// Tämä on vaikea konsepti, mutta tosi tärkeä kun päästään Reactin maailmaan.

