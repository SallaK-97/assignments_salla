const playerCount = 1
const isStressed = true
const hasIcecream = true
const sunShine = true
const noRain = true
const temperature = 20
const suzy = true
const dan = false
const day = "tuesday"

//a
if (playerCount === 4) {
    console.log("true")
}
else {
    console.log("false")
}
//b
if (!isStressed || hasIcecream) {
    console.log("Mark is happy")
}
else {
    console.log("Mars is not happy")
}
//c
if (sunShine && noRain && temperature >= 20) {
    console.log("It is a beach day")
}
else {
    console.log("it's not beach day")
}
//d
if (day === "tuesday" && (suzy || dan) && !(suzy && dan)) {
    console.log("happy")
}
else {
    console.log("unhappy)")
}

