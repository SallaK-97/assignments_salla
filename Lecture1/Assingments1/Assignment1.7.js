let price = 10
let discount = 1
let disPercent = discount / price * 100
let disPrice = price - discount
// Mulle jää nyt tästä epäselväksi, mikä on disPercent, jos tehtävänannon mukaan discount on alennusprosentti

console.log("Original price", price, "€", "discount percentage", disPercent, "%", "discount price", disPrice, "€")