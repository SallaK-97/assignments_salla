const a = 10
const b = 5

console.log("a + b =", a + b)
console.log("a - b =", a - b)
console.log("a / b =", a / b)
console.log("a * b =", a * b)

console.log("a**b =", a ** b)
console.log("a%b =", a % b)
console.log("(a+b)/2 =", (a + b) / 2)