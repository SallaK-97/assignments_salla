import express, { Request, Response } from "express";

const server = express();
server.use(express.json());

const username = "admin";
const password = "password"; // Tehtävänanto: "Remember that secret information needs to be provided via environment variables."

let notes: { id: number; title: string; content: string }[] = []

const authenticate = (req: Request, res: Response, next: any) => {
  const authHeader = req.headers["authorization"];
  if (authHeader) {
    const encodedCredentials = authHeader.split(" ")[1];
    const decodedCredentials = Buffer.from(encodedCredentials, "base64").toString("utf-8");
    const [authUsername, authPassword] = decodedCredentials.split(":");
    
    if (authUsername === username && authPassword === password) {
      return next();
    }
  }
  res.status(401).json({ error: "Unauthorized" });
};

server.post("/notes", authenticate, (req, res) => {
  const { id, title, content } = req.body;
  const note = { id, title, content };

  notes.push(note);
  res.status(201).json(note);
});

server.get("/notes", authenticate, (req, res) => {
  res.json(notes);
});

server.delete("/notes/:id", authenticate, (req, res) => {
  const id = req.params.id;
  const noteIndex = notes.findIndex((note) => note.id.toString() === id);

  if (noteIndex !== -1) {
    const deletedNote = notes.splice(noteIndex, 1);
    res.json(deletedNote[0]);
  } else {
    res.status(404).json({ error: "Note not found" });
  }
});


server.listen(3000, () => {
  console.log("Server running on port 3000");
});

// Tehtävänannosta:
// Add secret note endpoints to your notice board.
// Nyt täällä on korvattu aikaisemmat endpoiintit turvatuilla sen sijaan, että ne olisi lisätty sinne