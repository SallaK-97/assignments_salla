import express, {Request, Response} from 'express';

const server = express();

server.get('/',(req: Request, res: Response) => {
    res.send("Hello from Docker")
})

const port = 3000;
server.listen(port, () => {
    console.log('Server listening on port', port);
});