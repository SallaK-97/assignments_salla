import express, { Request, Response } from "express"

const server = express()
server.use(express.json())

let notes: { id: number; title: string; content: string }[] = [] // interface

server.post('/notes', (req, res) => {// Tyypit puuttuu. Nää on ainakin mun editorissa punasella alleviivattuna. Opettele pitämään huolta siitä, että koodissa ei ole punaista.
  const { id, title, content } = req.body
  const note = { id, title, content } // interface

  notes.push(note)
  res.status(201).json(note)
})

server.get('/notes', (req, res) => {
  res.json(notes)
})

server.delete('/notes/:id', (req, res) => {
	const id = req.params.id
  	const noteIndex = notes.findIndex((note) => note.id.toString() === id) // käytä mielummin find kuin findIndex

  if (noteIndex !== -1) {
    const deletedNote = notes.splice(noteIndex, 1)
    res.json(deletedNote[0])
	
  } else {
    res.status(404).json({ error: 'Note not found' })
  }
})

server.listen(3000, () => {
	console.log("Server running on port 3000")
})