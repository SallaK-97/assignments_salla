import Instrument from "./Instrument"
import violin from "./violin.png"
import guitar from "./guitar.png"
import tuba from "./tuba.png"
// Kuvat kannattaa pistää omaan kansioon

function App() {

  return (
    <div className='App'>
      <h1>Instruments</h1>
      <Instrument name={'violin'} price={1000} img={violin}/>
      <Instrument name={'guitar'} price={500} img={guitar}/>
      <Instrument name={'tuba'} price={800} img={tuba}/>
    </div>
  )
}

export default App
