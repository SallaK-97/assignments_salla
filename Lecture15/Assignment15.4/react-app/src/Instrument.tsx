interface Props {
    name: string
    price: number
    img: string

}

const Instrument = (props: Props) => {
    const string = `Instrument name: ${props.name}, Price: ${props.price} `
    return (
        <div className='Instrument'>
            {string}
            <img src={props.img} />
        </div>
    )
}
export default Instrument