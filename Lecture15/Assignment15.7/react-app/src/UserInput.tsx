import { useState, ChangeEvent } from 'react'

function UserInput() {
    const [name, setName] = useState('') // kenen/minkä nimi tää on?
    const [submit, setSubmit] = useState(name) // submit on vähän erikoinen nimi tälle muuttujalle, kun se on verbi


    const onNameChange = (event: ChangeEvent<HTMLInputElement>) => {
        setName(event.target.value)
    }
    const handleClick = () => {setSubmit(name)}

    return (
        <div className='Input'>
            <h2>Viesti: {submit}</h2>
            <input value={name} onChange={onNameChange} />
            <button onClick={handleClick}> Submit </button>
        </div>
    )
}

export default UserInput