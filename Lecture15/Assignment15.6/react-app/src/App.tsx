import { useState } from 'react'

function App() {
  const [countOne, setCountOne] = useState(0)
  const [countTwo, setCountTwo] = useState(0)
  const [countThree, setCountThree] = useState(0)
  const result = `${countOne + countTwo + countThree}`
  
  return (
    <>
      <h1>Counter</h1>
      <div className="Buttons">
        <button onClick={() => setCountOne((countOne) => countOne + 1)}>
          count is {countOne}
        </button>  
        <button onClick={() => setCountTwo((countTwo) => countTwo + 1)}>
          count is {countTwo}
        </button>  
        <button onClick={() => setCountThree((countThree) => countThree + 1)}>
          count is {countThree}
        </button>  
        <div> {result} </div>
      </div>
    </>
  )
}

export default App
