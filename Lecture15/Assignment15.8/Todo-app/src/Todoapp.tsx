import { useState } from "react";

interface TodoItem {
    text: string;
    isDone: boolean;
}

const Todoapp = () => {
    const [item, setItem] = useState<string>("");
    const [addNewItem, setAddNewItem] = useState<TodoItem[]>([]);

    const addItem = () => {
        if (item !== "") {
            setAddNewItem([...addNewItem, { text: item, isDone: false }]);
            setItem("");
        }
    };

    const deleteTodo = (text: string) => {
        const newTodos = addNewItem.filter((item) => {
            return item.text !== text; // poistaa kaikki samannimiset kerralla
        });
        setAddNewItem(newTodos);
    };

    const toggleTodo = (text: string) => {
        const updateTodos = addNewItem.map((todo) => {
            if (todo.text === text) { // kuin myös tämä
                return { ...todo, isDone: !todo.isDone };
            }
            return todo;
        });
        setAddNewItem(updateTodos);
    };

    return (
        <div className="Todoapp">
            <h2>Todo-list</h2>
            <input type="text" name="item" value={item} placeholder="Create new item" onChange={(event) => {
                setItem(event.target.value);
            }}
            />

            <button className="add button" onClick={addItem}>Add</button>

            {addNewItem?.length > 0 ? (
                <ul className="todo-list">
                    {addNewItem.map((todo, index) => (
                        <li key={index}>
                            {todo.text}
                            <input className="checkbox" type="checkbox" checked={todo.isDone} onChange={() => toggleTodo(todo.text)} />
                            <button className="delete button" onClick={() => {
                                deleteTodo(todo.text)
                            }}>Delete
                            </button>
                        </li>
                    ))}
                </ul>
            ) : null}
        </div>
    );
};

export default Todoapp;

// Selkeä tiedosto, helppo nähdä mitä missäkin tapahtuu.
