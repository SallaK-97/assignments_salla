import Todoapp from "./todoapp"
function App() {

  return (
    <>
     <Todoapp />
    </>
  )
}

export default App

// Tää on turha tiedosto tässä välissä nyt. Voit joko laittaa perustoiminnallisuuden App-komponenttiin, tai sitten ottaa mainissa suoraan käyttöön Todoapp-komponentin