import React, { useState } from 'react';
import './TicTacToe.css';

interface Square {
  id: number;
  value: string | null;
}

interface TicTacToeState {
  board: Square[];
  currentPlayer: string;
  gameEnded: boolean;
}

const initialBoard: Square[] = Array(9).fill({ id: 0, value: null });

const TicTacToe: React.FunctionComponent = () => {
  const [state, setState] = useState<TicTacToeState>({
    board: initialBoard,
    currentPlayer: 'X',
    gameEnded: false,
  });

  const handleClick = (id: number) => {
    if (state.gameEnded || state.board[id].value !== null) {
      return;
    }

    const updatedBoard = state.board.map((square, index) => {
      if (index === id) {
        return { ...square, value: state.currentPlayer };
      }
      return square;
    });

    setState((prevState) => ({
      ...prevState,
      board: updatedBoard,
      currentPlayer: prevState.currentPlayer === 'X' ? 'O' : 'X',
      gameEnded: checkGameEnded(updatedBoard),
    }));
  };

  const checkGameEnded = (board: Square[]): boolean => {
    const winningCombinations = [
      [0, 1, 2], [3, 4, 5], [6, 7, 8], // Horizontally
      [0, 3, 6], [1, 4, 7], [2, 5, 8], // Vertically
      [0, 4, 8], [2, 4, 6], // Diagonally
    ];

    for (const combination of winningCombinations) {
      const [a, b, c] = combination;
      if (
        board[a].value !== null &&
        board[a].value === board[b].value &&
        board[a].value === board[c].value
      ) {
        return true; // Game ended with a winner
      }
    }

    // Check if the board is full (all squares are filled)
    if (board.every((square) => square.value !== null)) {
      return true; // Game ended in a draw
    }

    return false; // Game not ended
  };

  // Tässä on hyvää kommenttien käyttöä, ne selventävät juuri sellaisia kohtia, jotka ei oo ihan selkeitä pelkkää koodia lukemalla.

  const resetGame = () => {
    setState({
      board: initialBoard,
      currentPlayer: 'X',
      gameEnded: false,
    });
  };

  return (
    <div className="tic-tac-toe">
      <h1>Tic Tac Toe</h1>
      <div className="board">
        {state.board.map((square, index) => (
          <div
            key={square.id}
            className={`square ${square.value ? 'occupied' : ''}`}
            onClick={() => handleClick(index)}
          >
            {square.value}
          </div>
        ))}
      </div>
      <div className="status">
        {state.gameEnded ? (
          <div className="game-over">
            {state.currentPlayer === 'X' ? 'O' : 'X'} wins!
          </div>
        ) : (
          <div className="current-player">
            Current Player: {state.currentPlayer}
          </div>
        )}
      </div>
      <button className="reset-button" onClick={resetGame}>
        Reset Game
      </button>
    </div>
  );
};

export default TicTacToe;

// Myöskin erinomainen koodi. Helppo lukea, selkeästi nimetyt ja jaotellut asiat. Sekä toimiva ja nätisti tyylitelty peli.
