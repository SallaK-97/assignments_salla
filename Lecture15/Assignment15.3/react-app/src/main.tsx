import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App.tsx'
import RandomNumber from './RandomNumber.tsx'

ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
  <React.StrictMode>
    <App />
    {/* <RandomNumber /> Tämä pois täältä */}
  </React.StrictMode>,
)
