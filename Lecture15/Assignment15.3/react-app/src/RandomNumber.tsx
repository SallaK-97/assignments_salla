const RandomNumber = () => {
    return (
        <div className='RandomNumber'>
           Your Random Number is: {Math.floor(Math.random() * (100-1))}
           {/* Tää arpoo nyt numeron väliltä 0-99, mikä ei oo tehtävän kannalta toki relevanttia, mutta FYI */}
        </div>
    )
}
export default RandomNumber