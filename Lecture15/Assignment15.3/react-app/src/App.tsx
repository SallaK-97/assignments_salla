import RandomNumber from "./RandomNumber"

function App() {

  return (
    <>
      <div className='Random number'>
        <h1> Random Number Generator</h1>
        <RandomNumber /> 
        {/* Täällä on sille oikea paikka, niin kaikki on nätisti Appin alla */}
      </div>
    </>
  )
}

export default App
