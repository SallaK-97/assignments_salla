function Namelist() {

    const people = [
      
        { name: <b>Ari </b>},
        { name: <i>Jari</i>},
        { name: <b>Kari</b>  },
        { name: <i>Sari</i> },
        { name: <b>Mari</b> },
        { name: <i>Sakari</i> },
        { name: <b>Jouko</b> },
      ]
      // Tämä ei nyt ota tehtävänannossa määritettyä listaa argumenttina, vaan tässä on kovakoodattuna muokattu lista.
      
      return (
        <div className='list'>
          {people.map(person => {
            return <div>
              Name: {person.name}
            </div>
          })}
        </div>
      )
    }
   

export default Namelist