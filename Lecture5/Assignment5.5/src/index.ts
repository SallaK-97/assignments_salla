//Assignment 5.5 Kesken. En saa tätä toimimaan. En ymmärrä miksi.

class Ingredient {
	name: string;
	amount: number;
	
	constructor(name: string, amount:number){
		this.name = name;
		this.amount = amount;
	}
	ingredientScale(factor:number) {
		this.amount = factor*this.amount;
	}
}
class Recipe {
	name: string;
	ingredients: Array<{ name: string, amount: number }>;
	servings: number;

	constructor(name: string, ingredients: Array<{ name: string, amount: number }>, servings: number){
		this.name = name;
		this.ingredients = ingredients;
		this.servings = servings;
	}
	toString () {
		return this.ingredients.reduce((acc: string, cur: { name: string, amount: number }) => {
			return acc + `- ${cur.name} (${cur.amount})\n`;
		}, `${this.name} (${this.servings} servings)\n\n`);
	}	
}
class HotRecipe extends Recipe {
	heatLevel: number;
  
	constructor(name: string, servings: number, ingredients: Ingredient[], heatLevel: number) {
		super(name, servings, ingredients); // täällä on punainen alleviivaus, joka kertoo, että sulla on ingredients ja servings väärässä järjestyksessä
		this.heatLevel = heatLevel;
	}
}

const flour = new Ingredient("flour", 300);
const water = new Ingredient("water", 150);
const oil = new Ingredient("Oil", 30);
const salt = new Ingredient("Salt", 0);
const chili = new Ingredient("chili", 1);
const tomatoes = new Ingredient ("tomatoes", 2);
const pepper = new Ingredient ("pepper", 3);

const tortillas = new Recipe("tortillas", [flour, water, oil, salt], 12);
const hotSauce = new HotRecipe("hot sauce", 3, [chili, tomatoes, pepper], 3);

console.log(hotSauce.toString());

console.log(tortillas.toString());