//Assignment 5.8

import fs from "fs";

const content  = JSON.parse(fs.readFileSync("./forecast_data.json", "utf-8"));

content.temperature = 10; 

fs.writeFileSync("./forecast_data.json", JSON.stringify(content));