//Assignment 5.11
const numbers:number[] =[1,9,5,3];

function aboveAverage (numbers: number[], start:number, end:number) {
	const sum = numbers.slice(start, end+1).reduce((a,b)=>a+b,0);
	const average = sum / numbers.length;
	const aboveAverageValue = numbers.filter(num => num > average);

	return aboveAverageValue;
}
const result: number[] = aboveAverage(numbers, numbers[0], numbers[numbers.length - 1]);
console.log(result);

// Tehtävänannossa: "Create a function that takes in an array and returns...". Tämä funktio ottaa nyt kolme parametria, pyydetyn arrayn ja lisäksi kaksi numeroa. Näiden numeroiden tarkoitus jää minulle koodista epäselväksi. Rivillä 5 niitä käytetään indekseinä, mutta rivillä 11 niiden parametreiksi annetaan arrayn arvoja?