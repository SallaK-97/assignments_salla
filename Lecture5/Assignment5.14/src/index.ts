//Assignment 5.14
const firstName = "Salla";
const lastName = "Kaikkonen";
const [username, password] = generateCredentials(firstName, lastName);

function generateCredentials(firstName: string, lastName: string): [string, string] {
	const currentYear = new Date().getFullYear().toString().slice(-2);
	const username = `B${currentYear}${firstName.slice(0, 2).toLowerCase()}${lastName.slice(0, 2).toLowerCase()}`;

	const randLetter = String.fromCharCode(65 + Math.floor(Math.random() * 26));
	const randSpecialChar = String.fromCharCode(33 + Math.floor(Math.random() * 15));
	const password = `${randLetter}${firstName.charAt(0).toLowerCase()}${lastName.charAt(lastName.length - 1).toUpperCase()}${randSpecialChar}${currentYear}`;
  
	return [username, password];
}
console.log(username, password);