import express, { Request, Response } from 'express'
import { addProduct, findProduct, findAll, deleteProduct, updateProduct } from './dao'
import { jest } from '@jest/globals'

const router = express.Router()

router.post('/', async (req: Request, res: Response) => {
    const { id, name, price } = req.body
    await addProduct(name, price)
    res.send({ id, name, price })
})
router.get('/:id', async (req: Request, res: Response) => {
    const { id } = req.params
    await findProduct(parseInt(id, 10))
    res.send({ id })

})
router.get('/', async (req: Request, res: Response) => {
    const products = await findAll()
    res.send(products)
})
router.delete('/:id', async (req: Request, res: Response) => {
    const { id } = req.params
    await deleteProduct(parseInt(id, 10))
    res.send('Product is deleted')
})
router.put('/:id', async (req: Request, res: Response) => {
    const { id } = req.params 
    const { name, price } = req.body 
    await updateProduct(parseInt(id, 10), name, price)
    res.send({id, name, price })
  });
export default router