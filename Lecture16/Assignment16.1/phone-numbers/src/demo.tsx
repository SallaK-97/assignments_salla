import React, { useState, ChangeEvent } from "react";

interface Props {
  addNumber: (number: string) => void;
}

function Input({ addNumber }: Props) {
  const [number, setNumber] = useState("");

  const onChange = (event: ChangeEvent<HTMLInputElement>) => {
    const inputNumber = event.target.value;
    // Remove any non-numeric characters from the input
    const sanitizedNumber = inputNumber.replace(/\D/g, "");

    if (sanitizedNumber.length === 10) {
      addNumber(sanitizedNumber); // Call the higher level function to add the number to the list
      setNumber(""); // Empty the input field
    } else {
      setNumber(sanitizedNumber);
    }
  };

  return (
    <div className="Input">
      <input
        type="tel" // Use type="tel" for better input support on mobile devices
        value={number}
        onChange={onChange}
        maxLength={10} // Limit input to ten characters
      />
    </div>
  );
}

function App() {
  const [numbers, setNumbers] = useState<string[]>([]);

  const addNumber = (number: string) => {
    setNumbers((prevNumbers) => [...prevNumbers, number]);
  };

  return (
    <>
      <Input addNumber={addNumber} />
      {numbers.map((number, index) => (
        <div key={index}>{number}</div>
      ))}
    </>
  );
}

export default App;
