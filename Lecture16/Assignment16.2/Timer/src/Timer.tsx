import { useState, useEffect } from 'react'

const Timer = () => {
    const [second, setSecond] = useState(0);

    useEffect(() => {
        const timer = setTimeout(() => {
            setSecond((prevSecond) => prevSecond + 1)
        }, 1000)

        return () => {
            clearTimeout(timer)
        }
    }, [second])

    return (
        <div>
            <h2> Timer: {second} </h2>
        </div>
    )
}
export default Timer