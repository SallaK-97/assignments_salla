//Assignment6.1

const anotherFunction = function(arg: number, func: (arg:number) => void){
	setTimeout(() => {
		func(arg);
	},arg);
};
anotherFunction(1000,() =>{
	console.log("3");
	anotherFunction(1000,() =>{
		console.log("2");
		anotherFunction(1000,() =>{
			console.log("1");
			anotherFunction(1000,() =>{
				console.log("GO!");

			});
		});
	});
});
