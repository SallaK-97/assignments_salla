/* eslint-disable no-mixed-spaces-and-tabs */
//Assignment6.9

function raceLap(): Promise<number> {
    return new Promise((resolve, reject) => {
        const crashChance = Math.random() * 100;
        if (crashChance <= 3) {
            reject("Crashed!");
        } else {
            const lapTime = Math.floor(Math.random() * 6 + 20); // floor tarvitaan vain, jos halutaan tasasekunteja
            resolve(lapTime);
        }
    });
}

function delay(ms: number): Promise<void> {
    return new Promise(resolve => setTimeout(resolve, ms));
}

async function race(drivers: string[], laps: number): Promise<string> { // tämä funktio itse asiassa palauttaa vaan stringin, joten Promise tässä kohtaa on turha
    const driversStats: { [key: string]: { totalTime: number, bestLapTime: number | null } } = {}; // tee tyypille oma interface

    for (const driver of drivers) {
        driversStats[driver] = { totalTime: 0, bestLapTime: null }; // null on tyyppinä ongelmallinen tässä, koska se ei ole number. käytä mielummin Infinity, joka on number, mutta suurempi kuin mikään numero, joten siihen voi tehdä vertailua.
    }
    // .map käyttöön

    for (let lap = 1; lap <= laps; lap++) {
        const lapPromises: Promise<void>[] = [];

        for (const driver of drivers) {
            const lapTime = await raceLap().catch((error) => {
                console.log(`Driver ${driver} crashed on lap ${lap}: ${error}`);
                return -1;
            });

            if (lapTime >= 0) {
                driversStats[driver].totalTime += lapTime;
                if (driversStats[driver].bestLapTime === null || lapTime < driversStats[driver].bestLapTime) { // tämä errori poistuu tolla Infinityllä
                    driversStats[driver].bestLapTime = lapTime;
                }
            }

            lapPromises.push(delay(1000)); 
        }

        await Promise.all(lapPromises);
    }

    let winner: string | null = null;
    let fastestTime = Infinity; // ja täällähän se jo on käytössä :)

    for (const driver of drivers) {
        if (driversStats[driver].bestLapTime !== null && driversStats[driver].totalTime < fastestTime) {
            fastestTime = driversStats[driver].totalTime;
            winner = driver;
        }
    }

    if (winner !== null) {
        return `The winner is ${winner} with a total time of ${fastestTime} seconds.`;
    } else {
        return "No winner. All drivers crashed.";
    }
}

const drivers = ["Driver 1", "Driver 2", "Driver 3"];
const laps = 5;

race(drivers, laps)
    .then((result) => {
        console.log(result);
    })
    .catch((error) => {
        console.log(error);
    });


// Hyviä vastauksia. Ekstratarkkuutta noihin tyypityksiin. Ota kaikkialla tyypit ja interfacet käyttöön, ja pidä huolta, että sulle on koko ajan selvää, että mikä minkäkin muuttujan tyyppi on. 