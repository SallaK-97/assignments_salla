//Assignment6.2

function delay(time: number, text: string): Promise<string>  {
	return new Promise((resolve) => {
		setTimeout(() => {
			resolve(text);
		}, time);
	});
}

console.log("3");

delay(1000, "2")
	.then(val => {
		console.log(val);
		return delay(1000, "1");
	})
	.then(val => {
		console.log(val);
		return delay(1000, "GO!");
	})
	.then(val => {
		console.log(val);
	});