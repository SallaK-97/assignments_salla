//Assignment6.6
function findMaxNumber (array: number[]): number {
	let max = array[0];
	for (let i = 1; i < array.length; i++ ) {
		if (array[i] > max) {
			max = array[i];
		}
	}
	return max;
}

function findSecondLargestNumber (array: number[]): number {
	let max = array[0];
	let secondLargest = array[1];

	if (secondLargest > max) {
		[max, secondLargest] = [secondLargest, max];
	}

	for (let i = 2; i < array.length; i++) {
		if (array[i] > max) {
			secondLargest = max;
			max = array[i];
		} else if (array[i] > secondLargest && array[i] < max) {
			secondLargest = array[i];
		}
	}

	return secondLargest;
}

const array = [1, 5, 8, 26, -3, 55];
const largest = findMaxNumber(array);
const secondLargest = findSecondLargestNumber(array);
console.log(largest);
console.log(secondLargest);
