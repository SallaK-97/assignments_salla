//Assignment6.7
const students = [
	{ name: "Markku", score: 99 },
	{ name: "Karoliina", score: 58 },
	{ name: "Susanna", score: 69 },
	{ name: "Benjamin", score: 77 },
	{ name: "Isak", score: 49 },
	{ name: "Liisa", score: 89 },
];

interface Student {
	name: string
	score: number
}

function findHighestScoringStudent (students: Student[]): Student {
	let highestStudent = students[0];

	for (let i = 1; i < students.length; i++) {
		if (students[i].score > highestStudent.score) {
			highestStudent = students[i];
		}
	}
	return highestStudent;
}

function findLowestScoringStudent (students: Student[]): Student {
	let lowestStudent = students[0];
	for (let i = 1; i < students.length; i++) {
		if (students[i].score < lowestStudent.score) {
			lowestStudent = students[i];
		}
	}
	return lowestStudent;
	
}

function calculateAverageScore(students: Student[]): number {
	let sum = 0;
	for (const student of students) {
		sum += student.score;
	}
	return sum / students.length;
}

// ^kaikissa kolmessa funktiossa reduce käyttöön!

function assignGrade (score: number): string {
	if (score >= 95 && score <= 100) {
		return "5";

	} else if (score >= 80 && score <= 94) {
		return "4";

	} else if (score >= 60 && score <= 79) {
		return "3";

	} else if (score >= 40 && score <= 59) {
		return "2";

	} else {
		return "1";
	}
}

const highestScoringStudent = findHighestScoringStudent(students);
const lowestScoringStudent = findLowestScoringStudent(students);
const averageScore = calculateAverageScore(students);

console.log("Highest Scoring Student:", highestScoringStudent);
console.log("Lowest Scoring Student:", lowestScoringStudent);
console.log("Average Score:", averageScore);

console.log("Students with score higher than average:");
for (const student of students) {
	if (student.score > averageScore) {
		console.log(student);
	}
}

console.log("Grades:");
for (const student of students) {
	const grade = assignGrade(student.score);
	console.log(`${student.name}: ${grade}`);
}

// Selkeästi jaoteltu koodi, helppo lukea