//Assignment6.3

const getValue = function (): Promise<number> {
	return new Promise((resolve, reject) => {
		setTimeout(() => {
			resolve(Math.random());
		}, Math.random() * 1500);
	});
};
const results = async () =>{
	const resultOne = await getValue();
	const resultTwo = await getValue(); 
	console.log(resultOne, resultTwo);
};
results();

let a = 0;
getValue()
	.then(val => {
		a = val;
		return getValue();
	})
	.then(val => {
		console.log("promise\t", a, val);
	});


