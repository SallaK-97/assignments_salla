import express, { Request, Response } from 'express';
import { middleware, unknownEndpoint } from './middlewares';

const server = express();
server.use(express.json());

server.use(middleware);

interface Student {
  id: string;
  name: string;
  email: string;
}

const students: Student[] = [];

server.use(express.json());

server.post('/student', (req: Request, res: Response) => {
	if (!req.body.id || !req.body.name || !req.body.email) {
		console.log('error 400');
		res.status(400).send('400 error');
	} else {
		const student: Student = {
			id: req.body.id,
			name: req.body.name,
			email: req.body.email,
		};
		students.push(student);
		res.status(201).send();
	}
});

server.get('/student/:id', (req: Request, res: Response) => {
	const id = req.params.id;
	const student = students.find((student) => student.id === id);

	if (student) {
		res.send(student);
	} else {
		res.status(404).send('Cant find student');
	}
});

server.get('/student', (req: Request, res: Response) => {
	const studenIds = students.map((ele) => ele.id);
	res.send(studenIds);
});

server.put('/student/:id', (req: Request, res: Response) => {
  const id = req.params.id;
  const { name, email } = req.body as Partial<Student>;
  const student = students.find((student) => student.id === id);
  if (!student) {
    res.status(404).send({ error: 'Student not found' });
  } else {
    if (!name && !email) {
      res.status(400).send({ error: 'Name or email is required' });
    } else {
      student.name = name || student.name;
      student.email = email || student.email;
      res.sendStatus(204);
    }
    console.log(student.id)
  }
});

server.delete('/student/:id', (req: Request, res: Response) => {
  const id = req.params.id;
  const index = students.findIndex((student) => student.id === id);
  if (index === -1) {
    res.status(404).send({ error: 'Student not found' });
  } else {
    students.splice(index, 1);
    res.sendStatus(204);
  }
});

server.use(unknownEndpoint);

server.listen(3000, () => {
  console.log('Server is running on port 3000');
});