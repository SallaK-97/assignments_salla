import { Request, Response, NextFunction} from "express"

export const validateBookData = (
	req: Request,
	res: Response,
	next: NextFunction ) => {
	const { id, name, author, read } = req.body
  
	if (!id || !name || !author || read === undefined) {
		return res.status(400).send("Invalid book data")
	}
  
	next()
}

export const logger = (req: Request, res: Response, next: NextFunction) => {
	//you can list what you want here
	const now = new Date()
	const method = req.method
	const url = req.url
	const body = req.body
	const params = req.params
    
	if (!body) {
		console.log("cant find body")
	} else {
		console.log(now.toLocaleString(), method, url, body, params)
	}
	next()
}

export const errorHandler = (_req: Request, res: Response) => {
	res.status(404).send("ERROR 404, NOT FOUND WHAT YOU ARE LOOKING FOR")
}