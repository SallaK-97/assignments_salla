//8.12

import express, { Request, Response } from "express"
import { validateBookData, errorHandler, logger } from "./middlewares"
import helmet from "helmet"

const server = express()
server.use(express.json())
server.use(helmet())

interface Book {
  id: number;
  name: string;
  author: string;
  read: boolean;
}

const books: Book[] = []

server.use(logger)

server.get("/api/v1/books", (_req: Request, res: Response) => {
	res.json(books)
})

server.get("/api/v1/books/:id", (req: Request, res: Response) => {
	const id = Number(req.params.id)
	const book = books.find((book) => book.id === id)

	if (book) {
		res.json(book)
	} else {
		res.status(404).send("Book not found")
	}
})

server.post("/api/v1/books", validateBookData, (req: Request, res: Response) => {
	const { id, name, author, read } = req.body

	const newBook: Book = {
		id: Number(id),
		name: String(name),
		author: String(author),
		read: Boolean(read),
	}

	books.push(newBook)
	res.status(201).send("Book created")
}
)

server.put("/api/v1/books/:id", validateBookData, (req: Request, res: Response) => {
	const id = Number(req.params.id)
	const bookIndex = books.findIndex((book) => book.id === id)  // pelkkä find() riittää tähän

	if (bookIndex === -1) {
		res.status(404).send("Book not found")
	} else {
		const { name, author, read } = req.body
		const updatedBook: Book = {
			id: id,
			name: String(name),
			author: String(author),
			read: Boolean(read),
		}

		books[bookIndex] = updatedBook // oikea kirja voidaan löytää IDn avulla, ei tarvita listan indexejä
		res.status(204).send()
	}
}
)

server.delete("/api/v1/books/:id", (req: Request, res: Response) => {
	const id = Number(req.params.id)
	const bookIndex = books.findIndex((book) => book.id === id)

	if (bookIndex === -1) {
		res.status(404).send("Book not found")
	} else {
		books.splice(bookIndex, 1)
		res.status(204).send()
	}
})

server.use(errorHandler)

server.listen(4000, () => {
	console.log("Server running on port 4000")
})