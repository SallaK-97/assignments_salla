import { executeQuery } from './db'

//------------------users---------------------------------------
export const addUser = async (username: string, full_name: string, email: string) => {
    const query = 'INSERT INTO users (username, full_name, email) VALUES ($1, $2, $3) RETURNING id'
    const params = [username, full_name, email]
    const result = await executeQuery(query, params)
    console.log("User added", result)
   
}
export const findOneUser = async (id: number) => {
    const query = 'SELECT id, username, full_name, email FROM users WHERE id=$1'
    const params = [id]
    const result = await executeQuery(query, params)
    console.log("One user's details \n", result)
    return result.rows[0];

}
export const findAllUsers = async () => {
    const query = 'SELECT id, username FROM users';
    const result = await executeQuery(query);
    const users = result.rows;
    console.log("All users found \n", users);
    return users;
}
//-----------------------posts----------------------------------------------------
export const addPost = async (title: string, content: string) => {
    const query = 'INSERT INTO posts (title, content, post_date) VALUES ($1, $2, NOW())'
    const params = [title, content]
    const result = await executeQuery(query, params)
    console.log("Post added", result)
   
}
export const findOnePost = async (id_users_post: number) => {
    const query = `
        SELECT 
            posts.id_users_post, 
            posts.title, 
            posts.content,
            posts.post_date, 
            comments.id_user_comment,
            comments.comment_post_title, 
            comments.comment_content,
            comments.comment_date
        FROM posts 
        LEFT JOIN comments ON posts.id_users_post = comments.id_user_comment
        WHERE posts.id_users_post = $1
    `;

    const params = [id_users_post];
    const result = await executeQuery(query, params);

    console.log("One posts' details \n", result);
    return result.rows[0];
}
export const findAllposts = async () => {
    const query = 'SELECT id_users_post, title FROM posts';
    const result = await executeQuery(query);
    const posts = result.rows;
    console.log("All posts found \n", posts);
    return posts;
}
//---------------------comment---------------------------------------------------------------
export const addComment = async (comment_post_title: string, comment_content: string) => {
    const query = 'INSERT INTO comments (comment_post_title, comment_content, comment_date) VALUES ($1, $2, NOW())'
    const params = [comment_post_title, comment_content]
    const result = await executeQuery(query, params)
    console.log("Comment added", result)
   
}

export const findAllCommentsByUser = async (id_user_comment: number) => {
    const query = `
      SELECT 
        id_user_comment,
        comment_post_title,
        comment_content,
        comment_date
      FROM comments
      WHERE id_user_comment = $1
    `;
  
    const params = [id_user_comment];
    const result = await executeQuery(query, params);
  
    return result.rows; 
  };
  
//----------------älä koske näihin-------------------------------------------------
/*
export const deleteProduct = async (id: number) => {
    const query = 'DELETE FROM products WHERE id=$1'
    const params = [id]
    const result = await executeQuery(query, params)
    return result

}
export const updateProduct = async (id: number, name: string, price: number) => {
    const query = 'UPDATE products SET name = $2, price = $3 WHERE id = $1'
    const params = [id, name, price]
    const result = await executeQuery(query, params)
    console.log("Product updated", result)
  }
*/