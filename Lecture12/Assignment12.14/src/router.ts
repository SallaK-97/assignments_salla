import express, { Request, Response } from 'express'
import { addUser, findOneUser, findAllUsers, addPost, findOnePost, findAllposts,addComment, findAllCommentsByUser } from './dao'

const router = express.Router()
//-----------------------users------------------------------
router.post('/users', async (req: Request, res: Response) => {
    const { username, full_name, email } = req.body
    await addUser(username, full_name, email)
    res.send({ username, full_name, email})
})
router.get('/users/:id', async (req:Request, res:Response) => {
    const {id} = req.params
    const parseId = parseInt(id, 10)
    const users = await findOneUser(parseId)
    res.send(users)
})
router.get('/users', async (req: Request, res: Response) => {
    const users = await findAllUsers()
    res.send(users)
})
//-------------------posts--------------------------------------------
router.post('/posts', async (req: Request, res: Response) => {
    const { title, content } = req.body
    await addPost(title, content)
    res.send({ title, content })
})

router.get('/posts/:id', async (req: Request, res: Response) => {
    const { id } = req.params;
    const parseId = parseInt(id, 10);
    const posts = await findOnePost(parseId);
    res.send({ posts });
  });
  
router.get('/posts', async (req: Request, res: Response) => {
    const posts = await findAllposts()
    res.send(posts)
})
//------------comments--------------------------------------------------------------------

router.post('/comment', async (req: Request, res: Response) => {
    const { comment_post_title, comment_content } = req.body;
    await addComment(comment_post_title, comment_content);
    res.send({ comment_post_title, comment_content });
  });
  

router.get('/comments/:id_user_comment', async (req: Request, res: Response) => {
    const { id_user_comment } = req.params;
    const comments = await findAllCommentsByUser(parseInt(id_user_comment, 10));
    res.send({ comments });
  });
  
  
export default router

//-----------älä koske näihin-------------------------------------
/*router.delete('/:id', async (req: Request, res: Response) => {
    const { id } = req.params
    await deleteProduct(parseInt(id, 10))
    res.send('Product is deleted')
})
router.put('/:id', async (req: Request, res: Response) => {
    const { id } = req.params 
    const { name, price } = req.body 
    await updateProduct(parseInt(id, 10), name, price)
    res.send({id, name, price })
  });*/