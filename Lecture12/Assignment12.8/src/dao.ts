import { executeQuery } from './db'

export const addProduct = async (name: string, price: number) => {
    const query = 'INSERT INTO products (name, price) VALUES ($1, $2) RETURNING id'
    const params = [name, price]
    const result = await executeQuery(query, params)
    console.log("debug", result)
    //return result.rows[0].id
}
export const findProduct = async (id: number) => {
    const query = 'SELECT id FROM products WHERE id=$1'
    const params = [id]
    const result = await executeQuery(query, params)
    console.log("product found \n", result)
    return result

}
export const findAll = async () => {
    const query = 'SELECT * FROM products';
    const result = await executeQuery(query);
    const products = result.rows;
    console.log("All products found \n", products);
    return products;
}
export const deleteProduct = async (id: number) => {
    const query = 'DELETE FROM products WHERE id=$1'
    const params = [id]
    const result = await executeQuery(query, params)
    return result

}
export const updateProduct = async (id: number, name: string, price: number) => {
    const query = 'UPDATE products SET name = $2, price = $3 WHERE id = $1'
    const params = [id, name, price]
    const result = await executeQuery(query, params)
    console.log("Product updated", result)
  }
