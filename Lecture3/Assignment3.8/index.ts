
const n = 17;
let i = 1;
let sum = 0;
while (i <= n) {
	if (i % 3 === 0 || i % 5 === 0) {
		sum += i;
	}
	i = i + 1;
}
console.log(sum);


