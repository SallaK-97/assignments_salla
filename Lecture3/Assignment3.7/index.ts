//For-loop

let n = 1;
let sum = 0;

for(sum = 0; n <= 5; n += 1){
    // for-loopissa kannattaa käyttää n++ notaatiota, joka on yleinen konventio
        sum = sum + n;
        console.log(sum)
    }

/*
 //while loop
const n = 5
let i = 1
let sum = 0

while (i <= n) {
    sum = sum + i
    i = i + 1
}
console.log(sum)*/