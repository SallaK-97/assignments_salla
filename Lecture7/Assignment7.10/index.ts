import { AzureFunction, Context, HttpRequest } from "@azure/functions";
import axios from "axios";

const httpTrigger: AzureFunction = async function (
  context: Context,
  req: HttpRequest
): Promise<void> {
  const requestNumber = context.bindings.invocationCount;

  let response;
  if ((requestNumber + 1) % 13 === 0) {
    response = await getRandomFoxImage();
  } else {
    const isCat = Math.random() < 0.5; 
    if (isCat) {
      response = await getRandomCatImage();
    } else {
      response = await getRandomDogImage();
    }
  }

  const htmlResponse = generateHTMLResponse(response.message, response.link);
  
  context.res = {
    headers: {
      "Content-Type": "text/html",
    },
    body: htmlResponse,
  };
};

async function getRandomCatImage() {
  const catResponse = await axios.get("https://cataas.com/cat");
  return {
    message: "Here's a random cat picture:",
    link: catResponse.request.res.responseUrl,
  };
}

async function getRandomDogImage() {
  const dogResponse = await axios.get("https://dog.ceo/api/breeds/image/random");
  return {
    message: "Here's a random dog picture:",
    link: dogResponse.data.message,
  };
}

async function getRandomFoxImage() {
  const foxResponse = await axios.get("https://randomfox.ca//?i=54");
  return {
    message: "Here's a random fox picture:",
    link: foxResponse.data.link,
  };
}

function generateHTMLResponse(message: string, imageUrl: string): string {
  return `
    <!DOCTYPE html>
    <html>
      <head>
        <title>Random Picture</title>
      </head>
      <body>
        <h1>${message}</h1>
        <img src="${imageUrl}" alt="Random Picture">
      </body>
    </html>
  `;
}

export default httpTrigger;