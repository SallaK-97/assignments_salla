import { calculator } from "../src/index";

test("dummy test", () => {
	expect(true).toBe(true);
});
test("multiplication", () => {
	expect(calculator("*",1,2) ).toBe(2);
});
test("borderline", () => {
	expect(calculator("*",-1, 2) ).toBe(-2);
});
test("difficult", () => {
	expect(calculator("*",10,0.5) ).toBe(5);
});