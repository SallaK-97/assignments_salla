
import { calculator } from "../src/index";

describe("divide", () => {
	it("divide 3", () => {
		const result = calculator("/",9,3);
		expect(result).toBe(3);
	});
	it(" borderline", () => {
		const result = calculator("/",-9,-3);
		expect(result).toBe(3);
	});
	it("difficult", () => {
		const result = calculator("/",10,0);
		expect(result).toBe("Cannot divide by zero");
	});
});