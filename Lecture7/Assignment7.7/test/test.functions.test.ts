
import { convert } from "../src"; 

describe("conversions test", () => {
	it("Conversion from deslitres to litres", () => {
		const result = convert( "2","dl","l");
		expect(result).toBe(0);
	});
	it("conversion from litres to cups", () => {
		const result = convert("3","l","cup");
		expect(result).toBe(13);
	});
	it("conversion from ounces to desilitres", () => {
		const result = convert("10.0","oz","dl");
		expect(result).toBe(3);
	});
	it("conversion from pints to ounces", () => {
		const result = convert("7","pint","oz");
		expect(result).toBe(112);
	});
	it("conversion from cups to litres", () => {
		const result = convert("5","cup","l");
		expect(result).toBe(1);
	});
});