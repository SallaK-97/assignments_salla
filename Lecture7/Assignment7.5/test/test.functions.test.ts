
import { calculator } from "../src/index";

describe("calculator test", () => {
	it("divide 3", () => {
		const result = calculator("/",9,3);
		expect(result).toBe(3);
	});
	it("borderline", () => {
		const result = calculator("/",-9,-3);
		expect(result).toBe(3);
	});
	it("difficult", () => {
		const result = calculator("/",10,0);
		expect(result).toBe("Cannot divide by zero");
	});
	it("plus", () => {
		const result = calculator("+",1,2);
		expect(result).toBe(3);
	});
	it("minus", () => {
		const result = calculator("-",3,2);
		expect(result).toBe(1);
	});
	it("multiplication", () => {
		const result = calculator("*",3,2);
		expect(result).toBe(6);
	});
	it("power", () => {
		const result = calculator("**",1,1);
		expect(result).toBe("Can't do that!");
	});
});