import request from 'supertest';
import server from '../src/server'; 

describe('User API', () => {
    describe('POST /register', () => {
      test('should register a new user', async () => {
        const response = await request(server)
          .post('/register')
          .send({ username: 'newuser', password: 'newpassword' });
        expect(response.status).toBe(201);
      });
  
      test('should return an error if registration fails', async () => {
        const response = await request(server)
          .post('/register')
          .send({ username: 'testuser', password: 'testpassword' });
  
        expect(response.status).toBe(201);
        // voit myös testata response.body ominaisuuksia, jossa pitäisi olla token menestyksekkään loginin tai registerin seurauksena
      });
    });


  describe('POST /login', () => {
    test('should login an existing user', async () => {
      const response = await request(server)
        .post('/login')
        .send({ username: 'testuser', password: 'testpassword' });
      expect(response.status).toBe(200);
    });
  
    test('should return an error if login fails', async () => {
      const response = await request(server)
        .post('/login')
        .send({ username: 'testuser', password: 'incorrectpassword' });
      
      expect(response.status).toBe(401);
    });
  });
});