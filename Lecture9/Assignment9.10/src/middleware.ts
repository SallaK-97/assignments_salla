import { Request, Response, NextFunction } from 'express';

export const logger = (req: Request, res: Response, next: NextFunction) => {
  console.log(`[${new Date().toISOString()}] ${req.method} ${req.url}`);
  next();
};

export const validateBookData = (req: Request, res: Response, next: NextFunction) => {
  const { id, name, author, read } = req.body;

  if (!id || !name || !author || read === undefined) {
    return res.status(400).send('Invalid book data');
  }

  next();
};

export const errorHandler = (err: any, req: Request, res: Response, next: NextFunction) => {
  console.error(err);
  res.status(500).send('Internal Server Error');
};