import express from 'express';
import { logger, errorHandler } from './middleware';
import booksRouter from './booksRouter';
import usersRouter from './userRouter';

const server = express();
server.use(express.json());
server.use(logger);

server.use('/api/v1/users', usersRouter);

server.use('/api/v1/books', booksRouter);
// Tokenia ei tarkisteta missään vaiheessa. Nyt endpointteihin pääsee käsiksi kuka tahansa.

server.use(errorHandler);

server.listen(3000, () => {
  console.log('Server running on port 3000');
});