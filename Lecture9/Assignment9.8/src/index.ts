import express from 'express';
import dotenv from 'dotenv';
import router from './userRouter';

dotenv.config();

const server = express();
server.use(express.json());

server.use(express.static('public'));


server.use('/', router);

const port = 3000;
server.listen(port, () => {
    console.log('Server listening on port', port);
});