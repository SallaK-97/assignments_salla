import express, { Request, Response } from 'express';
import argon2 from 'argon2'

export const userRouter = express.Router()

interface User {
    username: string;
    password: string;
  }
  
  const userStorage: User[] = [];

userRouter.post('/register', (req: Request, res: Response) => {
    const user = req.body
	console.log(user.username, user.password);

    const password = user.password
    argon2.hash(password)
    .then(result => console.log(result))

    userStorage.push(user.username, user.password)
    // Nyt tässä käy niin, että toi argon hashaa sen salasanan ja loggaa sen, mutta tulosta ei tallenneta mihinkään
    // push-komento sen sijaan tallentaa hashaamattoman plaintext-salasanan
    console.log(user.username, user.password)
    res.status(201).send()
    console.log("debug", userStorage)
});
export default userRouter 