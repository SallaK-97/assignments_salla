import 'dotenv/config'
import jwt from 'jsonwebtoken'

const payload = { username: 'testiuser' }
const secret = process.env.SECRET ?? ""
const options = { expiresIn: '10min'}

const token = jwt.sign(payload, secret, options)
console.log(token)