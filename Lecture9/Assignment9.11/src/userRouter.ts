import express, { Request, Response } from 'express';
import argon2 from 'argon2';
import jwt from 'jsonwebtoken';
import dotenv from 'dotenv';

dotenv.config();

const usersRouter = express.Router();

interface User {
  username: string;
  password: string;
  isAdmin: boolean;
}

const users: User[] = [];

users.push({
  username: process.env.ADMIN_USERNAME || 'admin',
  password: process.env.ADMIN_PASSWORD || 'adminpassword',
  isAdmin: true,
});

usersRouter.post('/register', async (req: Request, res: Response) => {
  const { username, password } = req.body;
  const existingUser = users.find((user) => user.username === username);
  if (existingUser) {
    console.log('Registration failed: Username already exists');
    return res.status(500).send();
  }

  try {
    const hash = await argon2.hash(password);
    const newUser: User = { username, password: hash, isAdmin: false };
    users.push(newUser);
    console.log('User registered:', newUser);
    res.status(201).json(newUser);
  } catch (error) {
    console.error('Error during password hashing:', error);
    res.status(500).send();
  }
});

usersRouter.post('/login', (req: Request, res: Response) => {
  const { username, password } = req.body;
  const user = users.find((user) => user.username === username);

  if (!user) { // Tarkista TypeScriptissä eksplisiittisesti (user === undefined)
    return res.status(401).send();
  }

  argon2
    .verify(user.password, password)
    .then((passwordMatches) => {
      if (passwordMatches) {
        const token = jwt.sign(
          { username: user.username, isAdmin: user.isAdmin },
          process.env.SECRET || '',
          {
            expiresIn: '15m',
          }
        );
        return res.status(200).json({ token });
      } else {
        return res.status(401).send();
      }
    })
    .catch((error) => {
      console.error('Error during password verification:', error);
      return res.status(500).send();
    });
});

usersRouter.post('/books', (req: Request, res: Response) => {
  const token = req.headers.authorization?.split(' ')[1];
  if (!token) {
    return res.status(401).send();
  }

  jwt.verify(token, process.env.SECRET || '', (err: any, decoded: any) => {
    if (err) {
      console.error('Error during token verification:', err);
      return res.status(500).send();
    }

    if (!decoded.isAdmin) {
      return res.status(403).send();
    }


    const { id, name, author, read } = req.body;
    const newBook = { id, name, author, read };
    console.log('New book added:', newBook);
    res.status(201).json(newBook);
  });
});
// Tässä on nyt ymmärretty ajatus hieman väärin. Tarkoitus on, että tarkistetaan tokenin olemassaolo, kun käyttäjä yrittää käyttää olemassaolevia, booksRouter tiedostossa olevia reittejä. Tätä varten kannattaa rakentaa middleware joka tarkistaa että token on validi, ja asettaa se niille reiteille, jotka halutaan suojattavaksi.

// olet jo tehnyt vastaavan mekanismin middlewarelle validateBookData, joten sitä voi käyttää mallina. Ajatuksena on, että mikäli mukana ei ole validia tokenia, middleware ei päästä pyyntöä eteenpäin, vaan lähettää virheviestin. 

export default usersRouter;
