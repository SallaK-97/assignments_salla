//En saa toimimaan 

import express from 'express';
import { logger, errorHandler } from './middleware';
import booksRouter from './booksRouter';
import userRouter from './userRouter';

const server = express();
server.use(express.json());
server.use(logger);

server.use('/api/v1/users', userRouter);

server.use('/api/v1/books', booksRouter);

server.use(errorHandler);

server.listen(3000, () => {
  console.log('Server running on port 3000');
});