import express, { Request, Response, NextFunction } from 'express'

export const middleware = (req: Request, res: Response, next: NextFunction) => {
  const time = new Date()
  
  const logger = {
    url: req.url,
    time: time, 
    metod : req.method,
    body: null
};
if(req.body) logger.body = req.body;
  console.log(logger);
  next()
}

export const unknownEndpoint = (req: Request, res: Response) => {
  res.status(404).send({ error: 'No one here' })
}
