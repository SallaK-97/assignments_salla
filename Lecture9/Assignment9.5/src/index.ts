//Kesken
import express from 'express';
import { middleware, unknownEndpoint } from './middlewares';
import router from './router'
import userRouter from './userRouter'
import "dotenv/config"

const server = express();
server.use(express.json());
server.use(express.static('public'))
server.use(middleware);
server.use('/student', router)
server.use(userRouter)
server.use(unknownEndpoint);

server.listen(3000, () => {
  console.log('Server is running on port 3000');
});