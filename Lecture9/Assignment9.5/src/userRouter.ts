import express, { Request, Response } from 'express';
import argon2 from 'argon2';

export const userRouter = express.Router();

interface User {
  username: string;
  password: string;
}

const userStorage: User[] = [];

userRouter.post('/register', (req: Request, res: Response) => {
  const user = req.body;
  console.log(user.username, user.password);

  const password = user.password;
  argon2.hash(password).then((result) => console.log(result));

  userStorage.push({ username: user.username, password: user.password });
  console.log(user.username, user.password);
  res.status(201).send();
  console.log('debug', userStorage);
});

// mitä loginille tapahtui?

userRouter.post('/admin', (req: Request, res: Response) => {
  const admin = req.body;
  const adminUsername = process.env.ADMIN_USERNAME;
  const adminPasswordHash = process.env.ADMIN_PASSWORD_HASH;

  if (admin.username === adminUsername && admin.password === adminPasswordHash) {
    // Käyttäjä ei lähetä hashia, vaan salasanan. Tän metodin pitää käyttää verify-funktiota ihan niinkuin loginissakin
    console.log('Admin login successful');
    res.sendStatus(204); 
  } else {
    console.log('Admin login unauthorized');
    res.sendStatus(401);
  }
});

export default userRouter;
