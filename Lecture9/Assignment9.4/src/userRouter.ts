import express, { Request, Response } from 'express';
import argon2 from 'argon2';

export const userRouter = express.Router();

interface User {
  username: string;
  password: string; // user-objektiin ei haluta tallentaa salasanaa, vaan hash, joten se kannattaa nimetä hashiksi
}

const userStorage: User[] = [];

userRouter.post('/register', (req: Request, res: Response) => {
  const user: User = req.body;
  // Koska toi request bodyn mukana tuleva data ei ole vielä "user", koska sitä ei oo varmistettu millään tavalla, kannattaa ottaa pelkät muuttujat sieltä ulos esim destrukturoimalla
  // const { username, password } = req.body
  // Sit näistä voi tehdä user-objektin

  console.log(user.username, user.password);

  const password = user.password;
  argon2
    .hash(password)
    .then((hashedPassword: string) => { // any tyypit pois
      user.password = hashedPassword;
      userStorage.push(user);
      console.log(user.username, user.password);
      res.status(201).send();
      console.log('debug', userStorage);
    })
    .catch((error: any) => { // okei, erroreissa any on sallittu, kun ei tyypitetä omia erroreita
      console.error(error);
      res.status(500).send('Internal Server Error');
    });
});

userRouter.post('/login', (req: Request, res: Response) => {
  const { username, password } = req.body;
  // jaha, ja täällähän näin on jo tehty... pitäs lukee koko filu ennen ku kirjottelee

  const user = userStorage.find((e) => e.username === username); // e --> user selkeämpi nimi

  if (user) {
    argon2
      .verify(user.password, password)
      .then((passwordMatch) => {
        if (passwordMatch) {
          console.log('Login successful');
          res.status(204).send();
        } else {
          console.log('Unauthorized: Incorrect password');
          res.status(401).send('Unauthorized');
        }
      })
      .catch((error) => {
        console.error(error);
        res.status(500).send('Internal Server Error');
      });
  } else {
    console.log('Unauthorized: User not found');
    res.status(401).send('Unauthorized');
  }
});

export default userRouter;
