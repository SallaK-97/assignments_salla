import 'dotenv/config'
import jwt from 'jsonwebtoken'

const payload = { username: 'testiuser' }
const secret = process.env.SECRET ?? ""
const options = { expiresIn: '1h'}

const token = jwt.sign(payload, secret, options)
console.log(token)
const myToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InRlc3RpdXNlciIsImlhdCI6MTY4NTM2MjgxOCwiZXhwIjoxNjg1MzYzNDE4fQ.a6-wMS3p1cbuJZ7hUhJrWIQ_ZykwBoZorwLTgGXtot0"
const result = jwt.verify(myToken, secret)
console.log(result)
