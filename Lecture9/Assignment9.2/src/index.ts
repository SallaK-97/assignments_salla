import express from 'express';
import { middleware, unknownEndpoint } from './middlewares';
import router from './router'


const server = express();
server.use(express.json());
server.use(express.static('public'))
server.use(middleware);
server.use('/student', router)
server.use(unknownEndpoint);

server.listen(3000, () => {
  console.log('Server is running on port 3000');
});