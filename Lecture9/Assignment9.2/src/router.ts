import express, { Request, Response } from 'express';
const router = express.Router()

interface Student {
    id: string;
    name: string;
    email: string;
  }
  
  const students: Student[] = [];

router.post('/', (req: Request, res: Response) => {
	if (!req.body.id || !req.body.name || !req.body.email) {
		console.log('error 400');
		res.status(400).send('400 error');
	} else {
		const student: Student = {
			id: req.body.id,
			name: req.body.name,
			email: req.body.email,
		};
		students.push(student);
		res.status(201).send();
	}
});

router.get('/:id', (req: Request, res: Response) => {
	const id = req.params.id;
	const student = students.find((student) => student.id === id);

	if (student) {
		res.send(student);
	} else {
		res.status(404).send('Cant find student');
	}
});

router.get('/', (req: Request, res: Response) => {
	const studenIds = students.map((ele) => ele.id);
	res.send(studenIds);
});

router.put('/:id', (req: Request, res: Response) => {
  const id = req.params.id;
  const { name, email } = req.body as Partial<Student>;
  const student = students.find((student) => student.id === id);
  if (!student) {
    res.status(404).send({ error: 'Student not found' });
  } else {
    if (!name && !email) {
      res.status(400).send({ error: 'Name or email is required' });
    } else {
      student.name = name || student.name;
      student.email = email || student.email;
      res.sendStatus(204);
    }
    console.log(student.id)
  }
});

router.delete('/:id', (req: Request, res: Response) => {
  const id = req.params.id;
  const index = students.findIndex((student) => student.id === id);
  if (index === -1) {
    res.status(404).send({ error: 'Student not found' });
  } else {
    students.splice(index, 1);
    res.sendStatus(204);
  }
});
export default router 